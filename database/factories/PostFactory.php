<?php
 
namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Post;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(5,10));
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'body' => $this->faker->paragraph(rand(200,500)),
            'precio' => $this->faker->randomDigit(1,10),
            'origen' => $this->faker->paragraph(rand(1,2)),
            'asientos' => $this->faker->paragraph(rand(1,2)),
            'category_id' => $this->faker->randomDigit(1,10),
            'user_id' => $this->faker->randomDigit(1,10),
        ];
    }
}
